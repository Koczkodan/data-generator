import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'notifications.settings')

app = Celery("notifications", broker="amqp://guest:guest@rabbitmq:5672")

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


app.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'notifications.celery.debug_task',
        'schedule': 30.0,
    },
    'generate-events': {
        'task': 'alerts.tasks.generate_alert',
        'schedule': 10.0,
    },
    'generate_csv': {
        'task':'alerts.tasks.create_csv',
        'schedule':30.0,
    }
}
app.conf.timezone = 'UTC'
