from faker import Faker
from alerts.models import *
from notifications.celery import app
import random
import json

@app.task()
def generate_alert():
    fake = Faker()
    obj, created = Alert.objects.get_or_create(
        PID=fake.pyint(max_value=100),
        alert_name=random.choice([
            "PID killed", 
            "PID restarted", 
            "PID attached",
            "PID created",
            "Test Alert",
        ]),
        alert_counter = Alert.objects.count()+1,
        aler_type_id=random.choice([AlertType.INTERNAL, AlertType.EXTERNAL]),
        defaults={
            "data": json.dumps(fake.pydict(value_types=str))
        }
    )

    if not created:
        obj.inc_counter()
        obj.save()

@app.task()
def create_csv():
    with open('./data.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['PID','aler_type','alert_name','data','alert_counter',"aler_type_id"])
        for alert in Alert.objects.all().values_list('PID','aler_type__name','alert_name','data','alert_counter',"aler_type_id"):
            writer.writerow(alert)



