from .models import Alert
from .tables import AlertTable

from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login/')
def homepage(request):
    table = AlertTable(Alert.objects.all())
    return render(request, 'alerts/index.html', {"table": table})

