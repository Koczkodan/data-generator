import django_tables2 as tables
from .models import Alert

class AlertTable(tables.Table):
    class Meta:
        model = Alert
