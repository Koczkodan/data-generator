# Generated by Django 2.0.13 on 2020-06-04 07:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alert',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alert_name', models.CharField(max_length=128)),
                ('data', models.TextField(null=True)),
                ('alert_counter', models.IntegerField(default=0)),
                ('PID', models.IntegerField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AlertType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.AddField(
            model_name='alert',
            name='aler_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='alerts.AlertType'),
        ),
    ]
