from django.db import models
import json,csv,os

class Extractor:
    keys_to_remove = ["password", "login", "key"]
    result = None
    def __init__(self, source):
        self.source = source  

    def extract(self):
        json_data = json.loads(self.source)
        for k in self.keys_to_remove:
            if k in json_data.keys():
                del json_data[k]
        self.result = json.dumps(json_data)


class AlertType(models.Model):
    INTERNAL, EXTERNAL = range(1, 3)

    name = models.CharField(max_length=128)


class Alert(models.Model):
    aler_type = models.ForeignKey(AlertType, on_delete=models.CASCADE)
    alert_name = models.CharField(max_length=128)

    data = models.TextField(null=True)
    alert_counter = models.IntegerField(default=0)
    PID = models.IntegerField(blank=True)

    def __str__(self):
        return self.alert_name

    def inc_counter(self):
        self.alert_counter +=1

    @classmethod
    def get_event_count_by_pid(self,objects,results={}):
        for elem in objects:
            if elem.aler_type_id not in results.keys():
                results.update({elem.aler_type_id: 0})
            results[elem.aler_type_id] += 1
        return results

    def is_internal(self):
        return self.aler_type_id == 1

    def is_external(self):
        return self.aler_type_id == 2

    def save(self, *args, **kwargs):
        try:
            ex = Extractor(self.data)
            ex.extract()
            self.data = ex.result
        except Exception as e:
            print(e)
            self.data = None
        super().save(*args, **kwargs)

