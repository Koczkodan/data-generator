from alerts.models import Extractor,AlertType,Alert
from django.test import TestCase
from alerts import tasks
import csv,os,json


class TestModels(TestCase):
    def test_csv_creation(self):
        self.alertType1 = AlertType.objects.create(
        )

        self.alert1 =Alert.objects.create(
            aler_type = self.alertType1,
            alert_name="PID killed",
            aler_type_id = 1,
            PID = "1",
            data=json.dumps({"password":"aaa","aa":"ss","kkk":"sss"})
            )
        
        tasks.create_csv()
        with open('./data.csv', 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
            for row in reader:
                pass
        self.assertEquals(row[0],self.alert1.PID)
        self.assertEquals(row[1],self.alert1.aler_type.name)
        self.assertEquals(row[2],self.alert1.alert_name)
        self.assertEquals(row[3],self.alert1.data)
        self.assertEquals(row[4],str(self.alert1.alert_counter))
        self.assertEquals(row[5],str(self.alert1.aler_type_id))
                
        self.assertEquals(os.path.isfile('./data.csv'),True)
        os.remove('./data.csv')
        self.assertEquals(os.path.isfile('./data.csv'),False)


    
    def test_generate_alert(self):
        tasks.generate_alert()
        tasks.generate_alert()
        self.obj = Alert.objects.last()

        self.assertEquals(Alert.objects.count(),2)
        self.assertEquals(self.obj.aler_type_id in [AlertType.INTERNAL, AlertType.EXTERNAL],True)
        self.assertEquals(self.obj.alert_name in [
            "PID killed", 
            "PID restarted", 
            "PID attached",
            "PID created",
            "Test Alert",
        ],True)
        self.assertEquals(self.obj.alert_counter,Alert.objects.count())