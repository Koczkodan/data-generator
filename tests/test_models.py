from alerts.models import Extractor,AlertType,Alert
from django.test import TestCase
import json


class TestModels(TestCase):
    
    def setUp(self):
        self.alertType1 = AlertType.objects.create(
        )

        self.alert1 =Alert.objects.create(
            aler_type = self.alertType1,
            aler_type_id = 1,
            PID = "1",
            data=json.dumps({"password":"aaa","aa":"ss","kkk":"sss"})
            )

        self.alert2 =Alert.objects.create(
            aler_type = self.alertType1,
            aler_type_id = 2,
            PID = "2",
            data=json.dumps({"key":"aaa","aa":"ss","kkk":"sss"})
        )

        self.alert3 =Alert.objects.create(
            aler_type = self.alertType1,
            aler_type_id = 1,
            PID = "3",
            data=json.dumps({"password":"aaa","aa":"ss","kkk":"sss"})
            
        )

    def test_alert_inc_counter(self):
        self.alert1.inc_counter()
        self.assertEquals(self.alert1.alert_counter,1)

    def test_alert_is_internal_external(self):
        
        self.assertEquals(self.alert1.is_internal(),True)
        self.assertEquals(self.alert2.is_internal(),False)
        self.assertEquals(self.alert1.is_external(),False)
        self.assertEquals(self.alert2.is_external(),True)

    def test_alert_save(self):

        self.assertEqual(self.alert1.data, json.dumps({"aa":"ss","kkk":"sss"}))

    def test_extractor(self):
        self.extractor = Extractor(json.dumps({"password":"aaa","aa":"ss","kkk":"sss"}))
        self.extractor.extract()
        self.assertEquals(self.extractor.result,json.dumps({"aa":"ss","kkk":"sss"}))

    def test_extractor_source(self):
        self.extractor = Extractor(json.dumps({"password":"aaa","aa":"ss","kkk":"sss"}))
        self.assertEquals(json.dumps({"password":"aaa","aa":"ss","kkk":"sss"}),self.extractor.source)

    def test_alert_str(self):
        self.assertEquals(self.alert1.alert_name,self.alert1.__str__())

    def test_get_event_count_by_pid(self):
        self.res = Alert.get_event_count_by_pid(Alert.objects.all())
        self.assertEquals(self.res,{1:2,2:1})







          

